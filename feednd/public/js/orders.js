/**
 * Created by Nick on 4/4/2016.
 */

"use strict";

$(function() {
    var firebaseOrder = new Firebase('https://web-apps-hw6.firebaseio.com/orders');
    var orderref;
    var order;
    var uid;

    $('#order-panel').hide();
    $('#btn-toggle-order').on('click', function(evt) {
       $('#order-panel').toggle();
    });

    firebaseOrder.onAuth(function (authResponse) {
        if (authResponse) {
            $('#btn-toggle-order').show();
            $('.btn-add-order').show();
            uid = authResponse.uid;
        } else {
            $('#btn-toggle-order').hide();
            $('#order-panel').hide();
            $('.btn-add-order').hide();
            uid = undefined;

            if (orderref !== undefined) {
                deleteOrder('');
            }
        }
    });

    var createOrder = function (uid) {
        order = { restaurant: '', items: [], num_items: 0, total: 0.00, submitted: false };
        orderref = firebaseOrder.child(uid).push(order);

        orderref.on('value', function(snapshot) {
            order.restaurant = snapshot.val().restaurant;
            order.total = snapshot.val().total;
            order.items = snapshot.val().items;
            order.num_items = snapshot.val().num_items;
            order.submitted = snapshot.val().submitted;
            updateOrderView();
            $('#order-panel').show();
        });
    };

    var deleteOrder = function(confirmText) {
        if (confirmText !== '' && !confirm(confirmText)) {
            return false;
        }

        orderref.remove();
        createOrder(uid);
        return true;
    };

    var pushOrderChanges = function (uid) {
        if (orderref === undefined) {
            createOrder(uid);
        }
        console.log(order);
        orderref.update(order);
    };

    var orderContains = function (restaurant, id) {
        if (order.restaurant === restaurant && order.items) {
            for (var i = 0; i < order.items.length; i++) {
                if (order.items[i].id === id) {
                    return order.items[i];
                }
            }
        }

        return false;
    };

    var removeOrderItem = function (index) {
        console.log('removing ' + index.toString());
        if (order.items === undefined || index >= order.items.length) {
            return;
        }

        if (confirm('Are you sure you want to remove ' + order.items[index].text + '?')) {
            console.log('removing for good');
            order.items.splice(index, 1);
        }

        updateOrderTotal();
        pushOrderChanges();
    };

    var submitOrder = function () {
        if ((order === undefined) || (order.items === undefined) || (order.items.length === 0)) {
            alert('Your order is empty!');
            return;
        }

        order.submitted = true;
        orderref.update(order);
        var total = order.total;
        orderref = undefined;
        order = undefined;

        updateOrderView();
        alert('Order submitted!  Your total was ' + total);
        $('#order-panel').hide();
    };

    var updateOrderItem = function(restaurant, itemnum, text, price, qty) {
        if (restaurant !== order.restaurant
            && !deleteOrder('You cannot place orders at more than one restaurant at once.  Would you like to delete your current order and start over?'))
        {
            return;
        }

        if (order.items === undefined) {
            order.items = [];
        }

        if (orderContains(restaurant, itemnum)) {
            for (var i = 0; i < order.items.length; i++) {
                if (order.items[i].id === itemnum) {
                    order.items[i].quantity = qty;
                }

                if (qty <= 0) {
                    order.items.splice(i, 1);
                }
            }
        } else if (qty > 0) {
            order.items.push({id: itemnum, text: text, price: price, quantity: qty});
        }

        updateOrderTotal();
        pushOrderChanges();
    };

    var updateOrderTotal = function() {
        order.num_items = 0;
        order.total = 0;
        for (var i = 0; i < order.items.length; i++) {
            order.total += order.items[i].price * order.items[i].quantity;
            order.num_items += order.items[i].quantity;
        }
    };

    var updateOrderView = function () {
        var $panel = $('#order-panel');
        var $itemList = $panel.find('ul').html('');
        $panel.find('#total-text').html((order !== undefined) ? order.total : 0);
        var $removeLink;

        if (order === undefined) {
            return;
        }

        for (var i = 0; i < order.items.length; i++) {
            $removeLink = $('<a href="#">remove</a>');
            $removeLink.data({index: i});
            $itemList.append('<li>' + order.items[i].quantity + ' x ' + order.items[i].text + '  -  ' + order.items[i].price + '</li>');
            $removeLink.on('click', function (evt) {
                removeOrderItem($(evt.target).data().index);
            });
            $itemList.append($removeLink);
        }
    };

    $('#btn-submit-order').on('click', submitOrder);

    setTimeout(function() {
    $('#restaurants').on('click', '.btn-add-order', function (evt) {
        evt.stopPropagation();

        var $target = $(evt.target);
        var menuItem = $target.data();
        if (order === undefined) {
            createOrder(uid);
            order.restaurant = menuItem.restaurant;
        }
        if (order.restaurant !== menuItem.restaurant &&
            !deleteOrder('You cannot place orders at more than one restaurant at once.  Would you like to delete your current order and start over?')) {
            return;
        }

        order.restaurant = menuItem.restaurant;
        var testItem = orderContains(menuItem.restaurant, menuItem.id);
        var $popup = $('<div class="order-qty-select"></div>');
        $popup.append('<p>How many ' + menuItem.text + '? </p>');
        $popup.append('<input type="text" id="qty-select" />');
        $('body').append($popup);
        $('#qty-select').val((testItem.quantity || 1).toString());
        $popup.dialog({
            modal: true,
            buttons: {
                'OK': function() {
                    var quantity = parseInt($('#qty-select').val()) || 0;
                    updateOrderItem(menuItem.restaurant, menuItem.id, menuItem.text, menuItem.price, quantity);
                    $(this).dialog('close');
                    $popup.remove();
                },
                'Cancel': function () {
                    $(this).dialog('close');
                    $popup.remove();
                }
            }
        });
    });
    }, 10);

    $('#order-panel-hide').on('click', function (evt) {
        $('#order-panel').hide();
    });
});