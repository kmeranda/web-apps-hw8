"use strict";

/**
 * Created by jesus on 3/17/16.
 */
function FeedND(fbname) {
    var firebase = new Firebase("https://" + fbname + ".firebaseio.com");
    this.firebase = firebase;
    var usersRef = this.firebase.child('users');
    var uid;
    var instance = this;
    this.usersRef = usersRef;
    var namesRef = this.firebase.child('names');
    this.namesRef = namesRef;

    //overridable functions
    this.onLogin = function (user) {};
    this.onLoginFailure = function () {};
    this.onLogout = function () {};
    this.onError = function (error) {};
    this.onSignupFailure = function() {};

    // long running firebase listener
    this.start = function () {
        firebase.onAuth(function (authResponse) {
            if (authResponse) {
                console.log("user is logged in");
                usersRef.child(authResponse.uid).once('value', function (snapshot) {
                    instance.user = snapshot.val();
                    instance.onLogin(instance.user);
                });
            } else {
                console.log("user is logged out");
                instance.onLogout();
            }
        });
    };

    this.uid = function () {
        return uid;
    };

    // signup
    this.signup = function (email, name, phone, street, city, state, zipcode, password) {
        this.firebase.createUser({
            email: email,
            name: name,
            phone: phone,
            street: street,
            city: city,
            state: state,
            zipcode: zipcode,
            password: password
        }, function (error, userData) {
            if (error) {
                instance.onError("Error creating user " + error);
            }
            else {
                instance.userData = userData;
                console.log("user data", userData);
                usersRef.child(userData.uid).set({
                    addr: {
                        city: city,
                        state: state,
                        street: street,
                        zip: zipcode
                    },
                    email: email,
                    name: name,
                    phone: phone
                }, function (error) {
                    if (error) {
                        instance.onError("User already exists, please try again!");
                        instance.onSignupFailure();
                    }
                    else {
                        instance.namesRef
                            .child(name)
                            .set(true);
                        instance.login(email, password);
                    }
                });
            }
        });
    };
    // login with email and password
    this.login = function (email, password) {
        this.firebase.authWithPassword({
            email: email,
            password: password
        }, function (error, authData) {
            if (!error) {
                instance.auth = authData;
                console.log("uid:", authData.uid);
            } else {
                instance.onError("login failed! " + error);
                instance.onLoginFailure();
            }
        }, {
            remember: "sessionOnly"
        });
    };
    // logout
    this.logout = function () {
        this.firebase.unauth();
        instance.auth = null;
    };

}

$(function () {
    // Use your firebase address here:
    var ll = new FeedND("web-apps-hw6");

    var $loginButton = $('#login-button'),
        $signupButton = $('#signup-button'),
        $logoutButton = $('#logout-button'),
        $loginForm = $('#login-form'),
        $signupForm = $('#signup-form'),
        $alerts;

    ll.onLogin = function (user) {
        console.log("in onLogin!");
        showAlert("Welcome to FeedND!", "success");
        $loginButton.hide();
        $signupButton.hide();
        $logoutButton.show();
        $signupForm.hide();
        $loginForm.hide();
    };

    ll.onLogout = function () {
            console.log("in onLogout");
            $loginButton.show();
            $signupButton.show();
            $logoutButton.hide();
            $loginForm.hide();
            $signupForm.hide();
    };

    ll.onLoginFailure = function () {
        console.log("in onLoginFailure");
        $loginButton.show();
        $signupButton.show();
    };

    $logoutButton.on('click', function (e) {
        var conf  = confirm("Are you sure you want to logout?");
        if (conf) {
            ll.logout();
            $logoutButton.hide();
            $loginButton.show();
            $signupButton.show();
            return false;
        }
    });


    ll.onError = function (error) {
        showAlert(error, "danger");
    };

    $loginButton.show();
    $signupButton.show();
    $logoutButton.hide();
    $loginForm.hide();
    $signupForm.hide();

    // login forms
    $loginButton.on('click', function (e) {
        console.log("clicked login button!");
        //$loginButton.hide();
        //$signupButton.hide();
        $signupForm.hide();
        $loginForm.show();
        $('#login-email').val("").focus();
        $('#login-password').val("").blur();
        return false;
    });

    $loginForm.on('submit', function (e) {
        $loginForm.hide();
        e.preventDefault();
        e.stopPropagation();
        ll.login($(this).find('#login-email').val(), $(this).find('#login-password').val());
        $('#login-email').val("").blur();
        $('#login-password').val("").blur();
        return false;
    });

    $signupButton.on('click', function (e) {
        console.log("clicked signup button");
        //$signupButton.hide();
        //$loginButton.hide();
        $loginForm.hide();
        $signupForm.show();
        $('#signup-email').val("").focus();
        $('#signup-password').val("").blur();
        //$('#signup-alias').val("").blur();
        return false;
    });

    $signupForm.on('submit', function (e) {
        $signupForm.hide();
        e.preventDefault();
        e.stopPropagation();
        var email = $(this).find('#signup-email').val(),
            name = $(this).find('#signup-name').val(),
            phone = $(this).find('#signup-phone').val(),
            street = $(this).find('#signup-street').val(),
            city = $(this).find('#signup-city').val(),
            state = $(this).find('#signup-state').val(),
            zip = $(this).find('#signup-zip').val(),
            //alias = $(this).find('#signup-alias').val(),
            password = $(this).find('#signup-password').val(),
            passwordConfirm = $(this).find('#password-confirm').val();
        if (password === passwordConfirm) {
            ll.namesRef.child(name).once('value',function(snapshot) {
                if (snapshot.val() === null) {
                    ll.signup(email, name, phone, street, city, state, zip, password);
                }
                else {
                    showAlert("Alias already taken, please choose again","danger");
                    $signupForm.show();
                    return false;
                }
            });
        } else {
            showAlert("Passwords do not match");
            $signupButton.show();
            $loginButton.show();
        }
        $('#signup-email').val("").blur();
        $('#signup-password').val("").blur();
        $('#password-confirm').val("").blur();
        //$('#signup-alias').val("").blur();
        return false;
    });

    function showAlert(message, type) {
        var $alert = (
            $('<div>')                // create a <div> element
                .text(message)          // set its text
                .addClass('alert')      // add some CSS classes and attributes
                .addClass('alert-' + type)
                .addClass('alert-dismissible')
                .hide()  // initially hide the alert so it will slide into view
        );

        /* Add the alert to the alert container. */
        $alerts = $('#alerts');
        $alerts.append($alert);

        $alerts.on('click', function (e) {
            var $t = $(e.target);
            $t.remove();
        });

        /* Slide the alert into view with an animation. */
        $alert.slideDown();
        setTimeout(function () {
            $alert.hide()
        }, 5000);
    }

    // ensure no user session is active
    ll.logout();
    // start firebase auth listener only after all callbacks are in place
    ll.start();
});